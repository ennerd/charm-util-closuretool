charm/util-closuretool
======================

Small tool to print information about a closure, for example when debugging.

Usage
-----

```php
$test = 123;
$closure = function($arg) use ($test) {};

$tool = new Charm\Util\ClosureTool($closure);
echo $tool."\n";
```

Output

```
function($arg) use($test=123) (in /root/charm-libs/charm-util-closuretool/test.php)
```

