<?php
require("vendor/autoload.php");

$test = 123;
$closure = function($arg) use ($test) {};

$tool = new Charm\Util\ClosureTool($closure);
echo $tool."\n";
