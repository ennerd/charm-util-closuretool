<?php
namespace Charm\Util;

use Closure;
use ReflectionFunction;

class ClosureTool {

    private Closure $closure;
    private ReflectionFunction $ref;

    public function __construct(Closure $closure) {
        $this->closure = $closure;
        $this->ref = new ReflectionFunction($closure);
    }

    public function getStartLine(): int {
        return $this->ref->getStartLine();
    }

    public function getEndLine(): int {
        return $this->ref->getEndLine();
    }

    public function getFile(): string {
        return $this->ref->getFileName();
    }

    public function getName(): string {
        return $this->ref->getName();
    }

    public function getSignature(bool $short=false): string {
        $signature = '';

        $obj = $this->ref->getClosureThis();
        if ($obj) {
            $signature .= get_class($obj).'::';
        }

        if ($this->ref->returnsReference()) {
            $signature .= '&';
        }

        if ($this->ref->getName() === '{closure}') {
            $signature .= 'function';
        } else {
            $signature .= $this->ref->getName();
        }

        $params = [];
        foreach ($this->ref->getParameters() as $param) {
            $s = '';
            if (!$short && $param->hasType()) {
                $s .= $param->getType().' ';
            }
            $s .= '$'.$param->getName();
            if ($param->isDefaultValueAvailable()) {
                if ($param->isDefaultValueConstant()) {
                    $s .= '='.$param->getDefaultValueConstantName();
                } else {
                    $s .= '='.$param->getDefaultValue();
                }
            }
            $params[] = $s;
        }
        $signature .= '('.implode(", ", $params).')';

        if (!empty($this->ref->getClosureUsedVariables())) {
            $used = [];
            foreach ($this->ref->getClosureUsedVariables() as $k => $v) {
                $s = '$'.$k.'=';
                if (is_scalar($v)) {
                    $s .= json_encode($v);
                } elseif (is_resource($v)) {
                    $s .= $v;
                } else {
                    $s .= "{".get_debug_type($v)."}";
                }
                $used[] = $s;
            }
            $signature .= ' use('.implode(", ", $used).')';
        }

        return $signature;
    }

    public function getBody(): string {
        
    }

    public function __toString(): string {
        return $this->getSignature().' (in '.$this->getFile().')';
    }

    public function __debugInfo() {
        return [
            'signature' => $this->getSignature(),
            'file' => $this->getFile(),
            'line' => $this->getStartLine()
        ];
    }
}
